# Blocks

Modular content engine for the Neoteric Design CMS

## Concepts and Features

### Content types

* Rich -- rich text
* Panorama -- single image
* Gallery -- multiple images
* Video -- embedded video from YouTube/Vimeo
* PullQuote -- specialized block quote rich text

### Assembling layout

Prior to 0.5.0, each block was designed to fill out one "row" on a page grid. As such we had Diptych and Triptych block types to have mutliple columns of text. As of version 0.5.0, we've divorced the content type from layout concerns. Blocks now have layout attributes like `arrangement`, `margin`, and `content-alignment`, along with the flexible content data.

The key attribute is `arrangement` which determines how much space a block takes up on a grid. We use few coarse grained attributes here to keep things simple both for us in designing for blocks, as well as the editor themself. Using the `arrangement` data, blocks are automatically set into rows for display.

* `full` -- the block takes up an entire row
* `half` -- the row is split between two blocks
* `float-left` & `float-right` -- the block is floated to one side of the prior full-width block.


```
+----------------------------------------------------------+
|                          Parent                          |
+----------------------------------------------------------+
|  +--Row-----------------------------------------------+  |
|  |  +--Block arrangment: full----------------------+  |  |
|  |  |                                              |  |  |
|  |  |                 {{ content }}                |  |  |
|  |  |                                              |  |  |
|  |  +----------------------------------------------+  |  |
|  +----------------------------------------------------+  |
|                                                          |
|                                                          |
|  +--Row-----------------------------------------------+  |
|  |  +--Block half-----------+--Block half----------+  |  |
|  |  |                       |                      |  |  |
|  |  |   {{ content }}       |    {{ content }}     |  |  |
|  |  |                       |                      |  |  |
|  |  +----------------------------------------------+  |  |
|  +----------------------------------------------------+  |
|                                                          |

|  +--Row-----------------------------------------------+  |
|  |  +--Block full----------------------------------+  |  |
|  |  |                         +--Block float-right-+  |  |
|  |  | Content can flow a-     |  {{ content }}     |  |  |
|  |  | round this floated      +--------------------+  |  |
|  |  | block. See watch. Lorem ipsum lorem lorem    |  |  |
|  |  +----------------------------------------------+  |  |
|  +----------------------------------------------------+  |
|                                                          |
+----------------------------------------------------------+
```

## Installation

Neoteric CMS gems are served from a private host. Replace `SECURE_GEMHOST` with the source address.

```ruby
# Gemfile
source SECURE_GEMHOST do
  gem 'blocks'
end
```

```sh
$ bundle install

$ rails g blocks:install

$ rake db:migrate
```

NOTE: As of 0.4.0, blocks requires JSONB support to handle content data, generially this means Postgres 9.4+.

### Configuration

# -- TODO --

## Usage
Blocks can be attached to any model

```ruby
# app/models/page.rb
class Page < Evergreen::Page
  has_blocks
end
```

### Building blocks

Blocks comes with (hopefully) everything you need to set up your form

* Assets: Include `blocks/builder.js` and `blocks/builder.scss`
* Strong parameters: adding `Blocks.params` to your parent models whitelist will take care of the appropriate nested attributes
* Rendering form: `<%= blocks_form(form) %>`

#### ActiveAdmin Example

Run the active_admin generator (automatically run with the default installer if you had ActiveAdmin installed).

```
$ rails g blocks:active_admin
```

Or, import the assets manually.`

```coffeescript
# app/assets/javascripts/active_admin.js.coffeescript

# ...
#= require blocks/builder
# ...
```

```scss
// app/assets/stylesheets/active_admin.css.scss

// ...
@import 'blocks/builder';
// ...
```

```ruby
# app/admin/pages.rb

ActiveAdmin.register Page do
  permit_params :title, :parent, Blocks.params
  # ...
end
```

```erb
<!-- app/views/admin/pages/_form.html.erb -->

<%= semantic_form_for [:admin, @page] do |f| %>
  <%= f.input :title %>
  <%= f.input :parent %>
  <!-- .... -->
  <%= blocks_form(f) %>
  <!-- .... -->
  <%= f.actions %>
<% end %>
```

### Rendering blocks

Use the `render_blocks_for` helper to output the blocks content for a parent.
Blocks includes some default Foundation-based styles for arrangement layouts (`@import "blocks/display";`), Neoteric FedKit will copy these into a project for you.

```erb
<%= render_blocks_for @page %>
```