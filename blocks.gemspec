$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "blocks/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "blocks"
  s.version     = Blocks::VERSION
  s.authors     = ["Madeline Cowie"]
  s.email       = ["madeline@cowie.me"]
  s.homepage    = "http://neotericdesign.com"
  s.summary     = "Neoteric Content Blocks"
  s.description = "Attach a variety of types of content to your model"
  s.license     = "Closed"

  s.files = Dir["{app,config,db,lib,vendor}/**/*", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", ">= 4.2.1"
  s.add_dependency "pg"

  s.add_development_dependency 'rspec-rails', '~> 3.6.0'
  s.add_development_dependency 'rspec-mocks'
  s.add_development_dependency "capybara"
end
