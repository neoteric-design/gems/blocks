# Blocks Changelog

# 0.6.0

* Ruby 3 fixes 

# 0.6.0.beta1

* Trial usage of forcing blocks partial path (Fix for rendering blocks in admin)

## 0.5.11

* Add `blocks:block` generator for scaffolding custom blocks

## 0.5.10

* Remove background/styling content attributes from searchable content by default

## 0.5.9

* Fix bug with pulling searchable content from nil content field

## 0.5.8

* Make available block layout options configurable
* Add experimental "thirds" layout rows
* Update UI


## 0.5.8.beta1

* Make available block layout options configurable
* Add experimental "thirds" layout rows

## 0.5.7

* Add default styles for blocks arrangments

## 0.5.6

* Versionify DB migrations for Rails 5.1
* Add active_admin generator for importing assets
* Fix: Permitted params on Panorama

## 0.5.5

* Fix typo in install generator

## 0.5.4

* Add contexts for image / gallery ui

## 0.5.3

* Update default gallery markup

## 0.5.2

* Fix bug in params generation

## 0.5.1

* Fix image block param whitelisting

## 0.5.0

* Add 'image' block for non-grid busting single image

## 0.5.0.rc4

* Updated gallery UI

## 0.5.0.beta12

* Updated UI to keep up with active admin skin

## 0.5.0.beta11

* Fixed: properly scope form  css as to not affect other checkboxes
* Added: blocks_searchable_content method

## 0.5.0.beta10

* Tidying up gallery UI

## 0.5.0.beta6

* WIP support for rendering floated blocks inside the template of its parent block

## 0.5.0.beta5
* New UI

## 0.5.0.beta2
* Set up scrapbook support for asset needing blocks

## 0.5.0.beta1

* Separate content types (Rich Gallery Video PullQuote from layout
* Add new block arrangement and layout options, "rows" logic
* Remove now redundant Diptych and Triptych block types

## 0.4.1

* Minor builder stylesheet fixes/enhancements

## 0.4.0

* Migrate individual block types having their own table to a single table
* Total rewrite of blocks form and JS for sanity and validity
* Remove hard dependency on Montage in prep for new asset manager
* Improved loading of helpers

## 0.3.0

* Rails 5 compatibility

## 0.2.2

* Minor fixes

## 0.2.0

* Allow passthrough config for Montage galleries

## 0.1.8

* Fix rendering unpersisted parent records

## 0.1.7

* Fix common n+1 query in `render_blocks_for` helper
