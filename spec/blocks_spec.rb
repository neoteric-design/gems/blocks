require "rails_helper"

describe Blocks do
  describe "#configure" do
    after :each do
      Blocks.reset
    end

    it "has default values" do
      expect(Blocks.configuration.editor).to eq(:text)
    end

    it "Saves configuration values" do
      Blocks.configure do |config|
        config.editor = :ckeditor
        config.block_types = %w(Rich Diptych)
      end
      expect(Blocks.configuration.editor).to eq(:ckeditor)
      expect(Blocks.configuration.block_types).to eq(['Rich', 'Diptych'])
    end

    it "Generates the proper filling params for inclusion in controllers" do
      Blocks.configure do |config|
        config.block_types = %w(Rich Diptych)
      end

      expect(Blocks.configuration.block_types).to eq(['Rich', 'Diptych'])
      expect(Blocks.send :content_params).to eq(Blocks::Rich.params + Blocks::Diptych.params)
    end

    it "Builds the proper params" do
      Blocks.configure do |config|
        config.block_types = %w(Rich)
        config.additional_params = [:slide_id]
      end

      expect(Blocks.params[0][:blocks_attributes]).to match_array(%i(slide_id position title id type _destroy body arrangement content_alignment margin ))
    end
  end
end