Blocks.configure do |config|
  # Formtastic Input you want to use for textareas,
  # change to :ckeditor if you're a fancy lad
  # config.editor = :text

  # Which blocks to activate for this app. Add your own!
  # config.block_types = %w(Rich Panorama Gallery Video PullQuote)
end