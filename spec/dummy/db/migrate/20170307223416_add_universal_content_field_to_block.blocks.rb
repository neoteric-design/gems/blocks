# This migration comes from blocks (originally 2)
class AddUniversalContentFieldToBlock < ActiveRecord::Migration[5.0]
  def change
    add_column :blocks_blocks, :content, :jsonb
    add_column :blocks_blocks, :type, :string
  end
end