require 'rails_helper'

RSpec.feature 'viewing blocks' do
  scenario do
    blocks = [
      Blocks::Rich.new(body: "rich", arrangement: 'full', position: 1),
      Blocks::PullQuote.new(body: "pull", arrangement: 'float_right', position: 2),
      Blocks::Rich.new(body: 'half-left', arrangement: 'half', position: 3),
      Blocks::Rich.new(body: 'half-right', arrangement: 'half', position: 4)
    ]
    parent = Parent.create! blocks: blocks

    visit parent_path(parent)

    within(".row:first-of-type .block-rich") do
      expect(page).to have_content('rich')
      within(".block-pull-quote") do
        expect(page).to have_content('pull')
      end
    end

    within(".row:nth-of-type(2)") do
      within(".block-rich:first-of-type") do
        expect(page).to have_content('half-left')
      end
      within(".block-rich:nth-of-type(2)") do
        expect(page).to have_content('half-right')
      end
    end
  end

  scenario "isolated float" do
    parent = Parent.create! blocks: [
      Blocks::Rich.new(body: 'hello', arrangement: 'float_left')
    ]

    visit parent_path(parent)

    expect(page).to have_content('hello', count: 1)
  end
end
