class ParentsController < ApplicationController
  before_action :find_parent, only: [:show, :edit, :update]

  def index
    @parents = Parent.all
  end

  def show
  end

  def edit
  end

  def new
    @parent = Parent.new
  end

  def create
    @parent = Parent.new(parent_params)
    if @parent.save
      redirect_to @parent, notice: "Saved!"
    else
      render :edit, error: "Error"
    end
  end

  def update
    if @parent.update(parent_params)
      redirect_to @parent, notice: "Saved!"
    else
      render :edit, error: "Error"
    end
  end

  private

  def find_parent
    @parent = Parent.find(params[:id])
  end

  def parent_params
    params.require(:parent).permit(:hero_id, gallery_ids: [])
  end
end