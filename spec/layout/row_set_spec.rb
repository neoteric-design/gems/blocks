require "rails_helper"

def map_arrangements(row_set)
  row_set.rows.map { |r| r.blocks.map(&:arrangement) }
end

module Blocks::Layout
  describe RowSet do
    describe "#build" do
      let(:row_set) { RowSet.new(block_set) }

      context "good block set" do
        let(:block_set) do
          [
            Blocks::Block.new(arrangement: 'full'),
            Blocks::Block.new(arrangement: 'half'),
            Blocks::Block.new(arrangement: 'half'),
            Blocks::Block.new(arrangement: 'full'),
            Blocks::Block.new(arrangement: 'float_right'),
            Blocks::Block.new(arrangement: 'float_right')
          ]
        end

        it 'compatible blocks are paired up' do
          row_set.build

          expect(map_arrangements(row_set)).to eq([['full'], ['half', 'half'], ['full', 'float_right', 'float_right']])
        end
      end

      context "float disconnected" do
        let(:block_set) do
          [
            Blocks::Block.new(arrangement: 'full'),
            Blocks::Block.new(arrangement: 'full'),
            Blocks::Block.new(arrangement: 'half'),
            Blocks::Block.new(arrangement: 'half'),
            Blocks::Block.new(arrangement: 'float_right')
          ]
        end

        it 'goes on its own row' do
          row_set.build

          expect(map_arrangements(row_set)).to eq([['full'], ['full'], ['half', 'half'], ['float_right']])
        end
      end

      context "divorced half" do
        let(:block_set) do
          [
            Blocks::Block.new(arrangement: 'full'),
            Blocks::Block.new(arrangement: 'half'),
            Blocks::Block.new(arrangement: 'full'),
            Blocks::Block.new(arrangement: 'half'),
            Blocks::Block.new(arrangement: 'float_right')
          ]
        end

        it 'each goes on their own row' do
          row_set.build

          expect(map_arrangements(row_set)).to eq([['full'], ['half'], ['full'], ['half'], ['float_right']])
        end
      end

      context "float before full" do
        let(:block_set) do
          [
            Blocks::Block.new(arrangement: 'float_right'),
            Blocks::Block.new(arrangement: 'full'),
            Blocks::Block.new(arrangement: 'half')
          ]
        end

        it 'goes on its own row, floats must be preceded by a full width' do
          row_set.build

          expect(map_arrangements(row_set)).to eq([['float_right'], ['full'], ['half']])
        end
      end
    end
  end
end
