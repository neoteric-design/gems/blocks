require "rails_helper"

class BlockHaver < ActiveRecord::Base
  has_blocks
end

describe "Active Record Extension" do
  it "provides friendly AR helper has_blocks" do
    expect(ActiveRecord::Base).to respond_to(:has_blocks)
    # bh = BlockHaver.new

    # expect(bh).to respond_to(:blocks)
  end

end