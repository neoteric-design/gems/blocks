require "blocks/engine"
require "blocks/configuration"
require "blocks/active_record"
require "blocks/sortable"
require "blocks/rebuildable"
require "blocks/layout"
require "blocks/exporters"

module Blocks
  class << self
    def table_name_prefix
      'blocks_'
    end

    def block_class_names
      configuration.block_types.map { |t| "Blocks::#{t}" }
    end

    def params
      [blocks_attributes: combined_params]
    end

    attr_writer :configuration
    def configuration
      @configuration ||= Configuration.new
    end

    def reset
      @configuration = Configuration.new
    end

    def configure
      yield(configuration)
    end
  end

  private
  def self.content_params
    block_class_names.map { |b| b.constantize.params }.flatten.uniq
  end

  def self.base_params
    %i(type position title arrangement margin content_alignment id _destroy)
  end

  def self.additional_params
    configuration.additional_params || []
  end

  def self.combined_params
    base_params.concat(content_params).concat(additional_params)
  end
end
