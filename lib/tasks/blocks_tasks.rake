namespace :blocks do
  namespace :upgrade do
    desc 'Migrate from < v0.4.0 tables to 0.4.x+ JSON fields'
    task migrate_to_json: :environment do
      %w(Rich Diptych Triptych Video Gallery Panorama PullQuote).each do |type|
        stub_class = Object.const_set("Stub#{type}", Class.new(ActiveRecord::Base) do
          self.table_name = "blocks_#{type.tableize}"
        end)

        stub_class.all.each do |legacy_filling|
          block = Blocks::Block.find_by(filling_id: legacy_filling.id, filling_type: "Blocks::#{type}")
          if defined?(Montage)
            Montage::Gallery
              .where(gallery_attachable_type: "Blocks::#{type}", gallery_attachable_id: legacy_filling.id)
              .update_all(gallery_attachable_type: "Blocks::Block", gallery_attachable_id: block.id)
          end
          block.content = legacy_filling.attributes.select { |k,v| Blocks.send(:content_params).map(&:to_s).include?(k) }
          block.type = block.filling_type
          puts block.inspect
          block.save!
        end
      end
    end

    desc 'Drop excess tables from < v0.4.0'
    task drop_legacy_tables: :environment do
      puts "Not Implemented Yet"
    end
  end

  namespace :export do
    desc "Export given classes to a body field"
    task :embellish, [:classes] => :environment do |t, args|
      args[:classes].split(' ').each do |class_name|
        klass = class_name.constantize

        puts "Exporting all #{class_name}..."
        klass.find_each do |record|
          Blocks::Exporters.export(record)
        end
      end
    end
  end
end
