module Blocks
  class Engine < ::Rails::Engine
    isolate_namespace Blocks

    initializer 'blocks.action_controller',
                :before => :load_config_initializers do
      ActiveSupport.on_load :action_controller do
        ::ActionController::Base.send(:helper, Blocks::ViewHelper)
        ::ActionController::Base.send(:helper, Blocks::BuilderHelper)
      end
    end

    initializer 'blocks.extend_active_record',
                :after => :load_config_initializers do
      ::ActiveSupport.on_load :active_record do
        ::ActiveRecord::Base.extend(::Blocks::ActiveRecord::ClassMethods)
      end
    end
  end
end
