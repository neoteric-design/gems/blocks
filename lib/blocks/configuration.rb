module Blocks
  class Configuration
    attr_accessor :editor, :block_types, :additional_params, :gallery_options,
                  :block_set_label, :arrangements, :content_alignments, :margins


    def initialize
      @editor ||= :text
      @block_types ||= %w(Rich Panorama Gallery Video PullQuote)
      @additional_params ||= []
      @gallery_options ||= Hash.new { |h,k| h[k] = {} }
      @block_set_label = "Blocks"
      @arrangements ||= Block.arrangements.keys
      @content_alignments ||= Block.content_alignments.keys
      @margins ||= Block.margins.keys
    end
  end
end
