module Blocks
  module Exporters
    def self.export(record, field = :body)
      record.update(field => record.blocks.map do |block|
        exporter = "Blocks::Exporters::#{block.block_name.classify}".constantize
        exporter.new(block).export
      end.join)
    end

    class Base
      attr_reader :block
      def initialize(block)
        @block = block
      end

      def export
        raise NotImplementedError
      end

      def path_for(record)
        Rails.application.routes.url_helpers.polymorphic_path(record, only_path: true)
      end

      def inline_storehaus_image(attachment)
        "<figure><img src='#{path_for(attachment.storehaus_variant)}' #{storehaus_transformation(attachment)}'></figure>"
      end

      def storehaus_transformation(attachment)
        return if attachment.storehaus_transformation.crop.blank?
        <<~ATTR
          data-storehaus-transformation="#{attachment.storehaus_transformation.to_json}"
        ATTR
      end
    end

    class Rich < Base
      def export
        block.body
      end
    end

    class Video < Base
      def export
        return if block.url.blank?
        "<oembed src='#{block.url}'></oembed>"
      end
    end

    class Image < Base
      def export
        return unless block.blocks_image.attached?
        inline_storehaus_image(block.blocks_image)
      end
    end

    class Gallery < Base
      def export
        return unless block.gallery_items.attached?
        ["<neoteric-gallery>", *image_tags, "</neoteric-gallery>"].join
      end

      def image_tags
        block.gallery_items.map { |image| inline_storehaus_image(image) }
      end
    end

    class PullQuote < Base
      def export
        """
          <blockquote class='blocks-#{block.color}'>#{block.body}</blockquote>
        """
      end
    end
  end
end
