module Sortable
  def self.included(base)
    base.send(:default_scope, -> { base.order('position') })
    base.send(:before_create, :set_position)
  end

  private
  def set_position
    self.position ||= next_position
  end

  def next_position
    max = self.class.maximum(:position) || -1
    # if there is no position yet,
    # -1 + 1 will start at 0
    max += 1
  end
end
