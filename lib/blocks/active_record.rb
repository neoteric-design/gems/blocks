module Blocks
  module ActiveRecord
    module ClassMethods
      def has_blocks
        include InstanceMethods
        has_many :blocks, dependent: :destroy, class_name: 'Blocks::Block',
                 as: :parent, inverse_of: :parent

        accepts_nested_attributes_for :blocks, allow_destroy: true
      end
    end

    module InstanceMethods
      def blocks_searchable_content
        blocks.map(&:searchable_content).join
      end
    end
  end
end
