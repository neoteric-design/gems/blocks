module Blocks
  module Layout
    class Row
      attr_reader :blocks
      def initialize(blocks = [])
        @blocks = blocks
      end

      def add_block(block)
        return false unless can_add_block?(block)
        @blocks.append(block)

        true
      end

      def primary_blocks
        if has_full_width_block?
          [blocks.detect(&:full?)]
        else
          blocks
        end
      end

      def secondary_blocks
        if has_full_width_block?
          floated_blocks
        else
          []
        end
      end

      private

      def can_add_block?(block)
        return false if totally_full?

        case block.arrangement
        when "full"
          empty?
        when "two_thirds"
          empty? || has_one_third_block?
        when "one_third"
          empty? || has_one_third_block? || has_two_thirds_block? || has_two_thirds_blocks?
        when "half"
          empty? || has_one_half_block?
        when /float.*/
          empty? || has_full_width_block?
        else
          false
        end
      end

      def floated_blocks
        blocks.select(&:float?)
      end

      def empty?
        blocks.empty?
      end

      def totally_full?
        has_two_halves?
      end

      def has_floats?
        blocks.any?(&:float?)
      end

      def has_full_width_block?
        blocks.any?(&:full?)
      end

      def has_one_half_block?
        blocks.count == 1 && blocks.first.half?
      end

      def has_two_halves?
        blocks.count == 2 && blocks.all?(&:half?)
      end

      def has_one_third_block?
        blocks.count == 1 && blocks.first.one_third?
      end

      def has_two_thirds_blocks?
        blocks.count == 2 && blocks.all?(&:one_third?)
      end

      def has_two_thirds_block?
        blocks.count == 1 && blocks.first.two_thirds?
      end
    end
  end
end
