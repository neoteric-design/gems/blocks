module Blocks
  module Layout
    class RowSet
      attr_accessor :blocks
      attr_reader :rows
      def initialize(blocks = [])
        @blocks = blocks
      end

      def build
        @rows = [Row.new]
        blocks.each do |block|
          unless @rows.last.add_block(block)
            @rows << Row.new([block])
          end
        end
        self
      end
    end
  end
end
