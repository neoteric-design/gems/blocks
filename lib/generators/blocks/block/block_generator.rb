require 'rails/generators'

module Blocks
  # Generate custom blocks for a project
  class BlockGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :class_name, type: :string, required: true
    argument :attributes, type: :array

    def generate_model
      template "model.rb.erb", "app/models/blocks/#{underscored_name}.rb"
    end

    def generate_form_fields
      template "form.html.erb",
               "app/views/admin/blocks/_#{underscored_name}_fields.html.erb"
    end

    def generate_view
      template "block.html.erb",
               "app/views/blocks/_#{underscored_name}.html.erb"
    end

    def add_to_initializer
      inject_into_file "config/initializers/blocks.rb", before: "\nend" do
        "\n  config.block_types << '#{cameled_name}'"
      end
    end

    private

    def underscored_name
      class_name.underscore
    end

    def cameled_name
      class_name.camelize
    end
  end
end
