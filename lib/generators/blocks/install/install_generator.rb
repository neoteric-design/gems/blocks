require 'rails/generators'

module Blocks
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :layout_name, :type => :string, :default => "application"

    def copy_templates
      copy_file "initializer.rb", "config/initializers/blocks.rb"
      copy_file "_gallery.html.erb", "app/views/blocks/_gallery.html.erb"
      copy_file "_image.html.erb", "app/views/blocks/_image.html.erb"
      copy_file "_panorama.html.erb", "app/views/blocks/_panorama.html.erb"
      copy_file "_video.html.erb", "app/views/blocks/_video.html.erb"
      copy_file "_rich.html.erb", "app/views/blocks/_rich.html.erb"
      copy_file "_pull_quote.html.erb", "app/views/blocks/_pull_quote.html.erb"
    end

    def scrapbook_setup
      return unless defined?(Scrapbook)
      contexts = [
        "  config.register_context :gallery_items, many: true, target_resolution: [800,0]",
        "  config.register_context :pullquote_background, many: false, target_resolution: [1200,0]",
        "  config.register_context :panorama, many: false, target_resolution: [1200,0]",
        "  config.register_context :blocks_image, many: false, target_resolution: [800,0]"
      ].join("\n")
      insert_into_file "config/initializers/scrapbook.rb", contexts, before: "end\n"
    end

    def generate_admin
      generate 'blocks:active_admin' if defined?(ActiveAdmin)
    end

    # def add_to_layout_stylesheet
    #   return unless layout_stylesheet
    #   prepend_to_file layout_stylesheet, %Q{@import "blocks/display";\n}
    # end

    def install_migrations
      rake("railties:install:migrations FROM=blocks")
    end

    private

    def stylesheet_extension
      %w(scss css.scss css).detect do |ext|
        File.exist? "app/assets/stylesheets/#{layout_name}.#{ext}"
      end
    end

    def layout_stylesheet
      return unless stylesheet_extension
      "app/assets/stylesheets/#{layout_name}.#{stylesheet_extension}"
    end
  end
end
