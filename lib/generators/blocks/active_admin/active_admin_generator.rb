require 'rails/generators'

module Blocks
  module ActiveAdminAssets
    def insert_into_css_file(content, **options)
      return unless css_file

      options[:after] ||= %r{@import "active_admin/base";}
      insert_into_asset_file css_file, content, **options
    end

    def insert_into_js_file(content, **options)
      content.gsub!(%r{^//}, '#') if js_file.ends_with?('coffee')
      content.gsub!(/^#/, '//') if js_file.ends_with?('js')

      options[:after] ||= %r{(//)|#= require +['"]?active_admin/base['"]?}
      insert_into_asset_file js_file, content, **options
    end

    private

    def js_file
      @js_file ||= find_existing_file(potential_js_files)
    end

    def css_file
      @css_file ||= find_existing_file(potential_css_files)
    end

    def find_existing_file(files)
      files.detect { |file| File.exist?(file) }
    end

    def insert_into_asset_file(asset_file, content, **options)
      return unless asset_file

      if File.binread(asset_file).include?(content)
        say_status('skipped', "insert into #{asset_file}", :yellow)
      else
        insert_into_file asset_file, "\n#{content}", **options
      end
    end

    def potential_js_files
      [
        'app/assets/javascripts/active_admin.coffee',
        'app/assets/javascripts/active_admin.js.coffee',
        'app/assets/javascripts/active_admin.js'
      ]
    end

    def potential_css_files
      [
        'app/assets/stylesheets/active_admin.scss',
        'app/assets/stylesheets/active_admin.css.scss',
        'app/assets/stylesheets/active_admin.css'
      ]
    end
  end

  class ActiveAdminGenerator < Rails::Generators::Base
    source_root File.expand_path('templates', __dir__)
    include ActiveAdminAssets

    def guard_against_no_activeadmin
      begin
        ActiveAdmin
      rescue StandardError
        say_status('quitting', 'you must install ActiveAdmin', :red)
        exit
      end

      unless js_file && css_file
        say_status('quitting', 'ActiveAdmin CSS/JS assets are missing. Did you run the generator?', :red)
        exit
      end
    end

    def config_activeadmin; end

    def require_js
      insert_into_js_file '//= require blocks/builder'
    end

    def require_css
      insert_into_css_file '@import "blocks/builder";'
    end
  end
end
