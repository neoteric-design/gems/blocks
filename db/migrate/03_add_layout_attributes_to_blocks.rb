class AddLayoutAttributesToBlocks < ActiveRecord::Migration[5.1]
  def change
    add_column :blocks_blocks, :margin, :integer, default: 1
    add_column :blocks_blocks, :arrangement, :integer, default: 0
    add_column :blocks_blocks, :content_alignment, :integer, default: 0
  end
end
