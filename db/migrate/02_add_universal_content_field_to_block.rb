class AddUniversalContentFieldToBlock < ActiveRecord::Migration[5.1]
  def change
    add_column :blocks_blocks, :content, :jsonb, default: {}
    add_column :blocks_blocks, :type, :string
  end
end