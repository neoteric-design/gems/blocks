class CreateBlocks < ActiveRecord::Migration[5.1]
  def change
    create_table :blocks_blocks do |t|
      t.string :title
      t.integer :position
      t.references :parent, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end