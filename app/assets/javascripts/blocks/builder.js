//= require spectrum

var Block = function(el){
  this.container = el;
  this.dispatcher = this.container;
  this.controls = {
    destroy: this.container.find('.control-destroy'),
    display: this.container.find('.control-display')
  };
  this.fieldset = this.container.find('.block-fields');
  this.fields = {
    destroy: this.fieldset.find('input.block-remove'),
    position: this.fieldset.find('input.block-position')
  };

  this.setHooks();
  this.dispatcher.trigger('blockAdded', [this]);
};

Block.prototype.remove = function(){
  this.container.addClass('removing');
  if(confirm('Are you sure you want to delete this?'))
    this.flagForDeletion();

  this.container.removeClass('removing');
};

Block.prototype.flagForDeletion = function(){
  this.container.addClass('flagged-for-deletion');
  this.fields.destroy.val('1');
  this.dispatcher.trigger('blockRemoved', [this]);
};

Block.prototype.collapse = function(){
  this.fieldset.addClass('collapse').fadeOut(400);
  this.controls.display.find('i')
    .removeClass('fa-chevron-down')
    .addClass('fa-chevron-right');
};

Block.prototype.expand = function(){
  this.fieldset.removeClass('collapse').fadeIn(400);
  this.controls.display.find('i')
    .addClass('fa-chevron-down')
    .removeClass('fa-chevron-right');
};

Block.prototype.toggleDisplay = function(){
  if(this.fieldset.hasClass('collapse')){
    this.expand();
  } else {
    this.collapse();
  }
};

Block.prototype.setPositionFromIndex = function() {
  index = this.container.index('.block-container');
  this.fields.position.val(index);
};

Block.prototype.setHooks = function(){
  this.controls.destroy.click({currentBlock: this}, function(e){
    e.data.currentBlock.remove();
    return false;
  });
  this.controls.display.click({currentBlock: this}, function(e){
    e.data.currentBlock.toggleDisplay();
    return false;
  });
};

Block.prototype.afterBuildHooks = function(){
  this.container.find('.uploader').each(function(){
    new Uploader(this);
  });

  this.container.find('.spectrum').spectrum();
};



var BlockSet = function(parentContainer) {
  window.ckeConfigs = [];
  this.parentContainer = $(parentContainer);
  this.resorting = false;
  this.blocks_added = false;
  this.blocks = [];
  this.forms = this.parentContainer.data('forms');
  this.controls = {
    addButtons: this.parentContainer.find('.add-blocks'),
    resort: this.parentContainer.find('.resort')
  };
  currentBlockSet = this;
  this.parentContainer.find('.block-container').each(function(i, el){
    currentBlockSet.blocks.push(new Block($(el)));
  });

  this.setHooks();
  this.sortify();
};

BlockSet.prototype.resort = function(){
  if(this.resorting)
    this.end_resort();
  else
    this.start_resort();
  this.resorting = !this.resorting;
};

BlockSet.prototype.start_resort = function(){
  var content = '<i class="fa fa-plus-square-o fa-fw"></i> Expand All';
  this.controls.resort.html(content);
  for(var i = 0; i < this.blocks.length; i++){
    this.blocks[i].collapse();
  }
};

BlockSet.prototype.end_resort = function(){
  var content = '<i class="fa fa-minus-square-o fa-fw"></i> Collapse All';
  this.controls.resort.html(content);
  for(var i = 0; i < this.blocks.length; i++){
    this.blocks[i].expand();
  }
};

BlockSet.prototype.updatePositions = function() {
  for(var i = 0; i < this.blocks.length; i++){
    this.blocks[i].setPositionFromIndex();
  }
};

BlockSet.prototype.build = function(type){
  this.blocks_added = true;
  content = this.forms[type];
  newId = new Date().getTime();
  regexp = new RegExp('new_block', 'g');
  $fresh = $(content.replace(regexp, newId)).hide();

  this.controls.addButtons.before($fresh);
  block = new Block($fresh);
  block.afterBuildHooks();
  this.blocks.push(block);
  $fresh.fadeIn(200);

  $fresh.find('.scrapbook.input').each(function(){
    window.scrapbooks.push(Scrapbook(this));
  });

  this.parentContainer.sortable('refresh');
  this.parentContainer.trigger('sortupdate');
};

BlockSet.prototype.setHooks = function(){
  this.controls.addButtons.find('a').click({blockSet: this}, function(e){
    blockType = $(this).data('block-type');
    e.data.blockSet.build(blockType);
    return false;
  });

  this.controls.resort.click({blockSet: this}, function(e){
    e.data.blockSet.resort();
    return false;
  });

  this.parentContainer.on('sortupdate', {blockSet: this}, function(e){
    e.data.blockSet.updatePositions();
  });
};

BlockSet.prototype.sortify = function() {
  this.parentContainer.sortable({
    forcePlaceholderSize: true,
    placeholder: "ui-state-highlight",
    items: '.block-container',
    axis: 'y',
    handle: '.block-controls',
    start: function(e, ui){
      $('textarea:not(.plaintext)', this).each(function(i){
        tagId = $(this).attr('id');
        ckeClone = $(this).next('.cke').clone().addClass('cloned');
        window.ckeConfigs[tagId] = CKEDITOR.instances[tagId].config;
        CKEDITOR.instances[tagId].destroy();
        $(this).hide().after(ckeClone);
      });
    },
    stop: function(e, ui){
      $('textarea:not(.plaintext)', this).each(function(i){
        tagId = $(this).attr('id');
        CKEDITOR.replace(tagId, window.ckeConfigs[tagId]);
        $(this).next('.cloned').remove();
      });
    }
 });
};

$(function(){
  window.block_set = new BlockSet($('.block-set'));

  $(document).ajaxSuccess(function(e, xhr, settings){
    // If fields are added and the form is saved via ajax, refresh the page to
    // avoid the added fields being re-added upon each save.
    if(window.block_set.blocks_added && settings.url.substring(0,8) != "/montage"){
      loc = location.href;
      idx_of_new = loc.search(/\/new(?=[^.]*$)/);
      idx_of_edit = loc.search(/\/edit(?=[^.]*$)/);
      if(idx_of_new != -1)
        location.href = loc.substring(0, idx_of_new);
      else if(idx_of_edit != -1)
        location.reload();
    }
  });
});
