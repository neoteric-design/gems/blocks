module Blocks
  module BuilderHelper
    def blocks_form(form)
      render('admin/blocks/form', f: form)
    end

    def block_types
      Blocks.configuration.block_types
    end

    def blocks_blank_form(form, block_type)
      new_block = Blocks::Block.new(type: "Blocks::#{block_type}")

      output = form.fields_for(:blocks, new_block, child_index: 'new_block') do |b|
        render("admin/blocks/block_fields", f: b)
      end

      output
    end

    def blocks_add_links(options = {})
      block_types.map do |block_type|
        link_to("Add #{block_type}", '#', options.merge(data: { block_type: block_type}))
      end.join.html_safe
    end

    def blocks_form_data(form)
      Hash[block_types.map do |block_type|
        [block_type, blocks_blank_form(form, block_type)]
      end]
    end

    def blocks_editor
      Blocks.configuration.editor
    end

    def block_set_label
      Blocks.configuration.block_set_label
    end

    def blocks_content_alignments
      Blocks.configuration.content_alignments.map { |ca| [fa_icon("alignment-#{ca}"), ca] }
    end

    def blocks_arrangements
      Blocks.configuration.arrangements.map { |ar| [fa_icon("arrangement-#{ar}"), ar] }
    end

    def blocks_margins
      Blocks.configuration.margins.map { |mar| [fa_icon("margin-#{mar}"), mar] }
    end
  end
end
