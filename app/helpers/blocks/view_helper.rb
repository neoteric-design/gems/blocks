module Blocks
  # Helpers for front-end Blocks
  module ViewHelper
    def render_blocks_for(parent, **options)
      build_row_set(parent.blocks).rows.each do |row|
        concat render_blocks_row(row, **options)
      end

      nil
    end

    def render_blocks_row(row, **options)
      row_options = { class: 'row' }.merge(options.delete(:row_options) || {})
      floated = render_blocks(row.secondary_blocks, **options)
      primary_options = options.merge(locals: { floated_blocks: floated })

      content_tag :div, row_options do
        render_blocks row.primary_blocks, **primary_options
      end
    end

    def render_blocks(blocks, **options)
      capture do
        blocks.each do |block|
          partial_path = "/#{block.to_partial_path}"
          concat render(options.merge(partial: partial_path, object: block))
        end
      end
    end

    def build_row_set(blocks)
      Blocks::Layout::RowSet.new(blocks).build
    end

    def css_classes_for_block(block)
      [
        'block',
        "block-#{block.block_name.dasherize}",
        "margin-#{block.margin}",
        "arrangement-#{block.arrangement}",
        "text-#{block.content_alignment}"
      ]
    end
  end
end
