module Blocks
  class Diptych < Block
    content_fields :left, :right
  end
end