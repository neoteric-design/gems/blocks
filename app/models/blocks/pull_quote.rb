module Blocks
  class PullQuote < Block
    content_fields :body, :color, :height, :width

    def self.searchable_fields
      [:body]
    end

    def background_image_url; end

    if respond_to?(:montage)
      montage :background
      def background_image
        background.key_image
      end

      def background_image_url
        background.key_image.url if background_image
      end

      def self.params
        [gallery_ids: []].concat(super)
      end
    end

    if const_defined?('Storehaus')
      # has_one_attached :pullquote_background

      # def self.params
      #   [:pullquote_background].concat(super)
      # end

    elsif respond_to?(:has_scrapbook)
      has_scrapbook :pullquote_background

      def self.params
        [:pullquote_background_id].concat(super)
      end

      def background_image_url
        pullquote_background&.file_url
      end
    end


    def css_style
      css_style = "background-image:url(#{background_image_url});height:#{height};background-color:#{color};"
      if width == 'full'
        css_style += "width:100%; left:0; position:fixed;"
      else
        css_style += "width:#{width};"
      end
    end

  end
end
