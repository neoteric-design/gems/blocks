module Blocks
  class Gallery < Block
    content_fields :style

    def self.searchable_fields
      []
    end

    if respond_to?(:montage)
      montage :gallery
      delegate :images, to: :gallery

      def self.params
        [gallery_ids: []].concat(super)
      end
    end

    if const_defined?('Storehaus')
      has_many_attached :gallery_items

      def self.params
        [gallery_items: []].concat(super)
      end

    elsif respond_to?(:has_scrapbook)
      has_scrapbook :gallery_items

      def self.params
        [gallery_item_ids: []].concat(super)
      end
    end
  end
end
