module Blocks
  class Block < ::ActiveRecord::Base
    default_scope lambda { order 'position ASC' }
    belongs_to :parent, polymorphic: true, touch: true, inverse_of: :blocks

    enum margin: { zero: 0, some: 1 }
    enum content_alignment: { left: 0, center: 1, right: 2 }
    enum arrangement: { full: 0, half: 1, float_left: 2, float_right: 3, one_third: 4, two_thirds: 5 }

    def self.content_fields(*fields)
      store_accessor :content, *fields
    end

    def self.params
      stored_attributes[:content]
    end

    def self.searchable_fields
      stored_attributes[:content]
    end

    def block_name
      type.demodulize.underscore
    end

    def float?
      float_left? || float_right?
    end

    def to_partial_path
      "blocks/#{block_name}"
    end

    def searchable_content
      self.class.searchable_fields.map do |field_name|
        content&.fetch(field_name.to_s)
      end.join
    end
  end
end

