module Blocks
  class Video < Block
    content_fields :url

    def embed_url
      @embed_url ||=  provider.nil? ? url : "#{sites[provider][:url]}#{embed_id}"
    end

    def provider
      @provider ||= sites.select { |k,v| url =~ /#{v[:regex]}/i }.keys.first
    end

    def embed_id
      @embed_id ||= url.scan(sites[provider][:regex]).flatten.first unless provider.nil?
    end

    def sites
      {
      'youtube' => {
          regex: /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/i,
          url:   "//www.youtube.com/embed/"
        },
      'vimeo'   =>  {
          regex: /.*vimeo.com.*\/(\d+)/i,
          url: "//player.vimeo.com/video/"
        }
      }
    end
  end
end
