module Blocks
  class Panorama < Block
    content_fields :style

    def self.searchable_fields
      stored_attributes[:content]
    end

    if respond_to?(:montage)
      montage :gallery

      def image
        gallery.key_image
      end

      def image_url(style = :original)
        image.url(style) if image
      end

      def self.params
        [gallery_ids: []].concat(super)
      end
    end

    if respond_to?(:has_scrapbook)
      has_scrapbook :panorama

      def self.params
        [:panorama_id].concat(super)
      end
    end
  end
end
