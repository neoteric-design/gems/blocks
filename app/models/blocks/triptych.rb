module Blocks
  class Triptych < Block
    content_fields :left, :right, :center
  end
end
