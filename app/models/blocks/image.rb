module Blocks
  class Image < Block
    content_fields :style

    def self.searchable_fields
      []
    end

    if const_defined?('Storehaus')
      has_one_attached :blocks_image

      def self.params
        [:blocks_image].concat(super)
      end

    elsif respond_to?(:has_scrapbook)
      has_scrapbook :blocks_image

      def self.params
        [:blocks_image_id].concat(super)
      end
    end
  end
end
